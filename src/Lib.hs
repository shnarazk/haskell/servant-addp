{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
module Lib
    (
      defaultMain
    ) where

import Control.Monad.Base ( MonadBase  )
import Control.Monad.Except ( ExceptT, MonadError )
import Control.Monad.IO.Class ( MonadIO )
-- import Control.Monad.Logger ( runStdoutLoggingT )
import Control.Monad.Reader ( MonadReader, ReaderT(ReaderT), runReaderT )
-- import Control.Monad.Trans.Control ( MonadBaseControl(StM, liftBaseWith, restoreM) )
import Data.Proxy ( Proxy(Proxy) )
import Data.Text ( Text )
import Network.Wai.Handler.Warp ( Port, run )
import Network.Wai.Middleware.RequestLogger ( logStdoutDev )
import Network.Wai ( Application )
import Servant
       ( Get, Handler(Handler), JSON, Post, ReqBody, ServantErr, Server, ServerT
       , (:>), (:<|>)((:<|>)), serve, hoistServer
       )
import System.Environment ( getEnv )

-------------------
-- Configuration --
-------------------

-- | This 'Config' object is used to store environment about our application.
-- It is created on startup and passed to all the handlers.
data Config = Config
  {
    configPort :: !Port            -- ^ 'Port' to listen on.
  }

-- | Create a 'Config' based on environment variables, using defaults if the
-- environment variables don't exist.
createConfigFromEnvVars :: IO Config
createConfigFromEnvVars = do
  port <- (\n -> if n == "" then  8080 else read n) <$> getEnv "PORT"
  pure Config { configPort = port }

------------------------------------
-- App-specific Monad and Actions --
------------------------------------

-- | This 'MyApiM' monad is the monad that will be used for running the web
-- application handlers.  This includes 'helloWorldHandler',
-- 'addTextHandler', and 'getTextsHandler'.
--
-- It is just a newtype around @'ReaderT' 'Config' ('ExceptT' 'ServantErr' 'IO')@.
newtype MyApiM a = MyApiM
  { unMyApiM :: ReaderT Config (ExceptT ServantErr IO) a
  } deriving ( Functor
             , Applicative
             , Monad
             , MonadBase IO
             , MonadError ServantErr
             , MonadIO
             , MonadReader Config
             )

---------------------
-- Web Application --
---------------------

-- | This 'API' type represents the three different routes for our web
-- application.
type API = "hello-world" :> Get '[JSON] Text
      :<|> "add-comment" :> ReqBody '[JSON] Text :> Post '[JSON] Text
      :<|> "get-comments" :> Get '[JSON] [Text]


-- | Create a WAI 'Application' capable of running with Warp.
app :: Config -> Application
app config = serve (Proxy :: Proxy API) apiServer
  where
    apiServer :: Server API -- = ServerT API Hander
    apiServer = hoistServer (Proxy :: Proxy API) transformation serverRoot
    transformation :: forall a . MyApiM a -> Handler a
    transformation = Handler . flip runReaderT config . unMyApiM
{-
app config = serve (Proxy :: Proxy API) apiServer
  where
    apiServer :: Server API
    apiServer = enter naturalTrans serverRoot

    naturalTrans :: MyApiM :~> Handler
    naturalTrans = NT transformation

    -- This represents a natural transformation from 'MyApiM' to 'Handler'.
    -- This consists of unwrapping the 'MyApiM', running the
    -- @'ReaderT' 'Config'@, and wrapping the resulting value back up in a
    -- 'Handler'.
    transformation :: forall a . MyApiM a -> Handler a
    transformation = Handler . flip runReaderT config . unMyApiM
-}

-- | Root of the web appliation.  This combines 'helloWorldHandler',
-- 'addTextHandler', and 'getTextsHandler'.
serverRoot :: ServerT API MyApiM
serverRoot = helloWorldHandler :<|> addTextHandler :<|> getTextsHandler

-- | Hello world handler.  Just returns the text @\"hello world\"@.
helloWorldHandler :: MyApiM Text
helloWorldHandler = pure "hello world"

-- | Adds a 'Text' to the database. Returns the 'Text' that was added.
addTextHandler :: Text -> MyApiM Text
addTextHandler comment = pure comment

-- | Returns all the 'Text's from the database.
getTextsHandler :: MyApiM [Text]
getTextsHandler = pure ["A text", "A message"]

----------
-- Main --
----------

defaultMain :: IO ()
defaultMain = do
  config <- createConfigFromEnvVars
  putStrLn $ "running servant-on-heroku on port " ++ show (configPort config) ++ "..."
  run (configPort config) . logStdoutDev $ app config
