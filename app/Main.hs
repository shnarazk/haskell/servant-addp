{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Data.IORef
import Prelude ()
import Prelude.Compat
import Control.Monad.Except
import Control.Monad.Reader
import Data.Aeson.Compat
import Data.Aeson.Types
import Data.Attoparsec.ByteString
import Data.ByteString (ByteString)
import Data.List
import Data.Maybe
import Data.String.Conversions
import Data.Time
import Data.Time.Calendar
import GHC.Generics
import Lucid
import Network.HTTP.Media ((//), (/:))
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import System.Directory
import Text.Blaze
import Text.Blaze.Html.Renderer.Utf8
import qualified Data.Aeson.Parser
import qualified Text.Blaze.Html
import Lib

data User = User
  { name :: String
  , age :: Int
  , email :: String
  , registration_date :: Day
  } deriving (Eq, Show, Generic)
instance ToJSON User

users1 :: [User]
users1 =
  [ User "Isaac Newton"    372 "isaac@newton.co.uk" (fromGregorian 1683  3 1)
  , User "Albert Einstein" 136 "ae@mc2.org"         (fromGregorian 1905 12 1)
  ]

type UserAPI1 = "users" :> Get '[JSON] [User]
                :<|> "add" :> Get '[JSON] [User]
                :<|> Get '[PlainText] String

server1 :: IORef [User] -> Server UserAPI1
server1 db = liftIO (readIORef db)
          :<|> do x <- liftIO (readIORef db)
                  let p = User "NEWONE" 372 "foo@are" (fromGregorian 1683  3 1)
                  liftIO (writeIORef db (p : x))
                  return (p : x)
          :<|> return "Hello\n"

{-
server1 :: Server UserAPI1
server1 = liftIO 
          :<|> do let p = User "NEWONE" 372 "foo@are" (fromGregorian 1683  3 1)
                  return (p : db)
          :<|> return "Hello\n"
-}

main :: IO ()
main = do
  db <- newIORef users1
  run 8081 $ serve (Proxy :: Proxy UserAPI1) (server1 db)
